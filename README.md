# UniThing

UniThing is a backend part of the social media Maven project using Java, Spring.

## Tech Stack

- Spring Boot
- Spring Boot Tomcat
- Spring Actuator
- Spring Jpa Hibernate
- Spring MVC
- Spring Security
- Postgres SQL
- Lombok
- JWT
- FlyWay

## Installation

``./gradlew clean install``
