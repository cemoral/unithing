FROM openjdk:14-jdk

EXPOSE 1010

ADD ./build/libs/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]