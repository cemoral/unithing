package com.csemapps.unithing.controller;

import com.csemapps.unithing.entity.User;
import com.csemapps.unithing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {

  @Autowired private UserService userService;

  @PostMapping
  public ResponseEntity createUser(@RequestBody User user) {
    return userService.createUser(user);
  }
}
