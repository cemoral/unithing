package com.csemapps.unithing.controller;

import com.csemapps.unithing.entity.dto.CommentDTO;
import com.csemapps.unithing.service.CommentService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

  @Autowired private CommentService commentService;

  @GetMapping("/post/{Id}")
  public ResponseEntity findCommentsByPost(@PathVariable("Id") String postId) {
    if (!StringUtils.isEmpty(postId)) {
      return ResponseEntity.ok(commentService.getCommentsByPost(postId));
    }
    return ResponseEntity.badRequest().build();
  }

  @PostMapping
  public ResponseEntity addComment(@RequestBody CommentDTO commentDTO) {
    if (null != commentDTO
        && !StringUtils.isEmpty(commentDTO.getPostId())
        && !StringUtils.isEmpty(commentDTO.getText())) {
      commentService.addComment(commentDTO);
      return ResponseEntity.ok().build();
    }
    return ResponseEntity.badRequest().build();
  }
}
