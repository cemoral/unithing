package com.csemapps.unithing.controller;

import com.csemapps.unithing.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/location")
@RequiredArgsConstructor
public class LocationController {

  private final LocationService locationService;

  @GetMapping
  public ResponseEntity<String> getCurrentCityLocation() {
    return ResponseEntity.ok(locationService.getCurrentCity());
  }
}
