package com.csemapps.unithing.controller;

import com.csemapps.unithing.entity.AuthenticationRequest;
import com.csemapps.unithing.jwt.JwtTokenProvider;
import com.csemapps.unithing.repository.UserRepository;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

  private final AuthenticationManager authenticationManager;

  private final JwtTokenProvider jwtTokenProvider;

  private final UserRepository userRepository;

  @Value("${security.jwt.token.expire-length:3600000}")
  private long validityInMilliseconds;

  @PostMapping("/signin")
  public ResponseEntity signin(@RequestBody AuthenticationRequest data) {
    try {
      String username = data.getUsername();
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(username, data.getPassword()));
      String token =
          jwtTokenProvider.createToken(
              username,
              userRepository
                  .findByUsername(username)
                  .orElseThrow(
                      () -> new UsernameNotFoundException("Username " + username + "not found"))
                  .getRoles());
      Map<Object, Object> model = new HashMap<>();
      Date expireDate = new Date(new Date().getTime() + validityInMilliseconds);
      model.put("username", username);
      model.put("token", token);
      model.put("expire", expireDate);
      return ResponseEntity.ok(model);
    } catch (AuthenticationException e) {
      throw new BadCredentialsException("Invalid username/password supplied");
    }
  }
}
