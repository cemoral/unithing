package com.csemapps.unithing.controller;

import com.csemapps.unithing.entity.dto.PostDTO;
import com.csemapps.unithing.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/posts")
public class PostController {

  @Autowired private PostService postService;

  @GetMapping
  public ResponseEntity findAll() {
    Page<PostDTO> page = postService.findAll();
    // HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/posts");
    return ResponseEntity.ok(page);
  }

  @GetMapping("/byDate/{id}/{pageNumber}/{pageSize}")
  public ResponseEntity<Page<PostDTO>> getPostsByLocationDate(
      @PathVariable String id, @PathVariable int pageNumber, @PathVariable int pageSize) {
    Page<PostDTO> posts =
        postService.getPostsByLocationDate(id, PageRequest.of(pageNumber, pageSize));
    return ResponseEntity.ok(posts);
  }

  @GetMapping("/byRelevance/{id}/{pageNumber}/{pageSize}")
  public ResponseEntity<Page<PostDTO>> getPostsByLocationRelevance(
      @PathVariable String id, @PathVariable int pageNumber, @PathVariable int pageSize) {

    Page<PostDTO> posts =
        postService.getPostsByLocationRelevance(id, PageRequest.of(pageNumber, pageSize));
    return ResponseEntity.ok(posts);
  }

  @PostMapping("/upVote/{Id}")
  public ResponseEntity setUpVote(@PathVariable String Id) {
    postService.setUpVote(Id);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/downVote/{Id}")
  public ResponseEntity setDownVote(@PathVariable String Id) {
    postService.setDownVote(Id);
    return ResponseEntity.ok().build();
  }

  @PostMapping
  public ResponseEntity newPost(@RequestBody PostDTO postDTO) {
    postService.newPost(postDTO);
    return ResponseEntity.ok().build();
  }

  @GetMapping("/user")
  public ResponseEntity findAllPostsByUser() {
    return ResponseEntity.ok().body(postService.findAllPostsByUser());
  }
}
