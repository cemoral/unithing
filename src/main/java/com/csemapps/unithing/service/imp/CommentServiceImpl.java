package com.csemapps.unithing.service.imp;

import com.csemapps.unithing.entity.Comment;
import com.csemapps.unithing.entity.Post;
import com.csemapps.unithing.entity.User;
import com.csemapps.unithing.entity.dto.CommentDTO;
import com.csemapps.unithing.repository.CommentRepository;
import com.csemapps.unithing.repository.PostRepository;
import com.csemapps.unithing.service.CommentService;
import com.csemapps.unithing.service.UserService;
import com.csemapps.unithing.utils.SecurityUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {

  @Autowired private CommentRepository commentRepository;

  @Autowired private PostRepository postRepository;

  @Autowired private UserService userService;

  @Override
  public List<CommentDTO> getCommentsByPost(String postId) {
    return commentRepository.findAllByPostIdOrderByCreateDateDesc(postId).stream()
        .map(c -> CommentDTO.builder().postId(postId).text(c.getText()).id(c.getId()).build())
        .collect(Collectors.toList());
  }

  @Override
  public void addComment(CommentDTO commentDTO) {
    Optional<Post> postOpt = postRepository.findById(commentDTO.getPostId());
    if (postOpt.isPresent()) {
      User user = userService.validateAndFindUser(SecurityUtils.getActiveUsername().get());
      commentRepository.save(
          Comment.builder().text(commentDTO.getText()).post(postOpt.get()).user(user).build());
    } else {
      log.debug("Post not found with id: {}", commentDTO.getPostId());
    }
  }
}
