package com.csemapps.unithing.service.imp;

import com.csemapps.unithing.entity.User;
import com.csemapps.unithing.exception.UserNotValidException;
import com.csemapps.unithing.exception.UsernameExistException;
import com.csemapps.unithing.repository.UserRepository;
import com.csemapps.unithing.service.UserService;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/** Service class for managing users. */
@Service
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

  @Autowired private UserRepository userRepository;

  @Autowired private PasswordEncoder passwordEncoder;

  @Override
  public ResponseEntity createUser(User newUser) {

    Optional<? extends User> userOpt = userRepository.findByUsername(newUser.getUsername());
    try {
      if (!userOpt.isPresent()) {
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        userRepository.save(newUser);
        return ResponseEntity.ok(
            String.format("User created with username: %s.", newUser.getUsername()));
      } else {
        throw new UsernameExistException("Username is already exists.");
      }
    } catch (UsernameExistException e) {
      return ResponseEntity.badRequest().body(e.getLocalizedMessage());
    } catch (Exception e) {
      log.error(e.getLocalizedMessage());
      return ResponseEntity.badRequest().body("Problem occurs when creating user.");
    }
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    return this.userRepository
        .findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
  }

  @Override
  public User validateAndFindUser(String username) {

    try {
      if (StringUtils.isEmpty(username)) {
        throw new UserNotValidException();
      }
      return userRepository
          .findByUsername(username)
          .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));

    } catch (Exception e) {
      log.error(e.getLocalizedMessage());
      return null;
    }
  }
}
