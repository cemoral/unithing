package com.csemapps.unithing.service.imp;

import com.csemapps.unithing.entity.Location;
import com.csemapps.unithing.entity.Post;
import com.csemapps.unithing.entity.User;
import com.csemapps.unithing.entity.dto.PostDTO;
import com.csemapps.unithing.exception.PostNotValidException;
import com.csemapps.unithing.repository.LocationRepository;
import com.csemapps.unithing.repository.PostRepository;
import com.csemapps.unithing.service.PostService;
import com.csemapps.unithing.service.UserService;
import com.csemapps.unithing.utils.SecurityUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class PostServiceImpl implements PostService {

  @Autowired private PostRepository postRepository;

  @Autowired private UserService userService;

  @Autowired private LocationRepository locationRepository;

  @Override
  public Page<PostDTO> findAll() {
    return new PageImpl<>(
        postRepository
            .findAll(Pageable.unpaged())
            .getContent()
            .parallelStream()
            .map(
                p ->
                    PostDTO.builder()
                        .downVote(p.getDownVote())
                        .upVote(p.getUpVote())
                        .location(p.getLocation())
                        .text(p.getText())
                        .id(p.getId())
                        .build())
            .collect(Collectors.toList()));
  }

  @Override
  public Page<PostDTO> getPostsByLocationDate(String locationName, Pageable pageable) {

    final Page<Post> pagePost =
        postRepository.findAllByLocation_NameOrderByCreateDateDesc(
            locationName.toUpperCase(), pageable);
    return new PageImpl<>(
        pagePost.stream()
            .map(
                p ->
                    PostDTO.builder()
                        .downVote(p.getDownVote())
                        .upVote(p.getUpVote())
                        .location(p.getLocation())
                        .text(p.getText())
                        .id(p.getId())
                        .createDate(p.getCreateDate())
                        .build())
            .collect(Collectors.toList()),
        pagePost.getPageable(),
        pagePost.getTotalElements());
  }

  @Override
  public Page<PostDTO> getPostsByLocationRelevance(String locationName, Pageable pageable) {

    final Page<Post> pagePost =
        postRepository.findAllByLocation_NameOrderByUpVoteDesc(
            locationName.toUpperCase(), pageable);
    return new PageImpl<>(
        pagePost.stream()
            .map(
                p ->
                    PostDTO.builder()
                        .downVote(p.getDownVote())
                        .upVote(p.getUpVote())
                        .location(p.getLocation())
                        .text(p.getText())
                        .id(p.getId())
                        .createDate(p.getCreateDate())
                        .build())
            .collect(Collectors.toList()),
        pagePost.getPageable(),
        pagePost.getTotalElements());
  }

  @Override
  @Transactional
  public void setUpVote(String postId) {
    Optional<Post> postOpt = postRepository.findById(postId);
    if (postOpt.isPresent()) {
      postOpt.get().setUpVote(postOpt.get().getUpVote() + 1);
    } else {
      log.debug("Post not found with id: {}", postId);
    }
  }

  @Override
  @Transactional
  public void setDownVote(String postId) {
    Optional<Post> postOpt = postRepository.findById(postId);
    if (postOpt.isPresent()) {
      postOpt.get().setDownVote(postOpt.get().getDownVote() + 1);
    } else {
      log.debug("Post not found with id: {}", postId);
    }
  }

  @Override
  @Transactional
  public void newPost(PostDTO postDTO) {
    try {
      if (null != postDTO
          && !StringUtils.isEmpty(postDTO.getText())
          && SecurityUtils.getActiveUsername().isPresent()) {
        User user = userService.validateAndFindUser(SecurityUtils.getActiveUsername().get());
        Optional<Location> locationOptional =
            locationRepository.findByName(postDTO.getLocation().getName().toUpperCase());
        Location loc =
            locationOptional.orElseGet(
                () ->
                    locationRepository.save(
                        Location.builder()
                            .name(postDTO.getLocation().getName().toUpperCase())
                            .build()));
        postRepository.save(
            Post.builder().text(postDTO.getText()).user(user).location(loc).build());
      } else {
        throw new PostNotValidException();
      }
    } catch (Exception e) {
      log.error(e.getLocalizedMessage());
    }
  }

  @Override
  public List<PostDTO> findAllPostsByUser() {
    if (SecurityUtils.getActiveUsername().isPresent()) {
      return postRepository
          .findAllByUserUsernameOrderByCreateDateDesc(
              SecurityUtils.getActiveUsername().get(), Pageable.unpaged())
          .stream()
          .map(
              p ->
                  PostDTO.builder()
                      .downVote(p.getDownVote())
                      .upVote(p.getUpVote())
                      .location(p.getLocation())
                      .text(p.getText())
                      .id(p.getId())
                      .createDate(p.getCreateDate())
                      .build())
          .collect(Collectors.toList());
    } else {
      return null;
    }
  }
}
