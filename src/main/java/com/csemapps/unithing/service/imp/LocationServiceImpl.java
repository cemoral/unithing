package com.csemapps.unithing.service.imp;

import com.csemapps.unithing.service.LocationService;
import java.nio.charset.StandardCharsets;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LocationServiceImpl implements LocationService {

  @Override
  public String getCurrentCity() {
    final HttpClient client = HttpClientBuilder.create().build();
    try (final CloseableHttpResponse response =
        (CloseableHttpResponse) client.execute(new HttpGet("https://geolocation-db.com/json"))) {
      final String responseBody =
          EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
      return responseBody;
    } catch (final Exception e) {
      log.error("An error occurred while parsing the response", e);
      return "";
    }
  }
}
