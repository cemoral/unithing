package com.csemapps.unithing.service;

import com.csemapps.unithing.entity.dto.CommentDTO;
import java.util.List;

public interface CommentService {

  List<CommentDTO> getCommentsByPost(String postId);

  void addComment(CommentDTO commentDTO);
}
