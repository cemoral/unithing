package com.csemapps.unithing.service;

import com.csemapps.unithing.entity.dto.PostDTO;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PostService {

  Page<PostDTO> findAll();

  Page<PostDTO> getPostsByLocationDate(String locationId, Pageable pageable);

  Page<PostDTO> getPostsByLocationRelevance(String locationId, Pageable pageable);

  void setUpVote(String postId);

  void setDownVote(String postId);

  void newPost(PostDTO postDTO);

  List<PostDTO> findAllPostsByUser();
}
