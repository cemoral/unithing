package com.csemapps.unithing.service;

import com.csemapps.unithing.entity.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

  ResponseEntity createUser(User newUser);

  User validateAndFindUser(String username);
}
