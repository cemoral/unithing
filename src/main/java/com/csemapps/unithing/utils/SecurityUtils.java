package com.csemapps.unithing.utils;

import java.util.Optional;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

  public static Optional<String> getActiveUsername() {
    if (null == SecurityContextHolder.getContext()
        || null == SecurityContextHolder.getContext().getAuthentication()) {
      return Optional.empty();
    }
    return Optional.of(SecurityContextHolder.getContext().getAuthentication().getName());
  }
}
