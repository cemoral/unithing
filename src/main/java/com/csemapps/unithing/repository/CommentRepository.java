package com.csemapps.unithing.repository;

import com.csemapps.unithing.entity.Comment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, String> {

  List<Comment> findAllByPostIdOrderByCreateDateDesc(String postId);
}
