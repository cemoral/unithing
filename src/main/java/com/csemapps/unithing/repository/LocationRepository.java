package com.csemapps.unithing.repository;

import com.csemapps.unithing.entity.Location;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, String> {

  Optional<Location> findByName(String name);
}
