package com.csemapps.unithing.repository;

import com.csemapps.unithing.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends PagingAndSortingRepository<Post, String> {

  Page<Post> findAllByLocation_NameOrderByCreateDateDesc(String locationName, Pageable pageable);

  Page<Post> findAllByLocation_NameOrderByUpVoteDesc(String locationName, Pageable pageable);

  Page<Post> findAllByUserUsernameOrderByCreateDateDesc(String username, Pageable pageable);
}
