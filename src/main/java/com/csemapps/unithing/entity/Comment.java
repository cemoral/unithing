package com.csemapps.unithing.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Comment extends BaseEntity {

  private static final long serialVersionUID = -7539886157431322765L;

  private String text;

  @ManyToOne
  @JoinColumn(name = "post_id_fk")
  @JsonIgnore
  private Post post;

  @ManyToOne
  @JoinColumn(name = "user_id_fk")
  @JsonIgnore
  private User user;
}
