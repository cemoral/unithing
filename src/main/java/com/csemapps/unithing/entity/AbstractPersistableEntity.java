package com.csemapps.unithing.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@MappedSuperclass
public abstract class AbstractPersistableEntity<ID> implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private ID id;

  @Version private Long version;
}
