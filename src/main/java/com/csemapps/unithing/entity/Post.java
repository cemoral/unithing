package com.csemapps.unithing.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Post extends BaseEntity {

  private static final long serialVersionUID = -9014125449031633168L;

  private String text;

  @Column(name = "up_vote")
  private int upVote;

  @Column(name = "down_vote")
  private int downVote;

  @ManyToOne
  @JoinColumn(name = "location_id_fk")
  @JsonIgnore
  private Location location;

  @ManyToOne
  @JoinColumn(name = "user_id_fk")
  @JsonIgnore
  private User user;

  @OneToMany(mappedBy = "post")
  private List<Comment> comments;
}
