package com.csemapps.unithing.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
@Data
class BaseEntity implements Serializable {

  private static final long serialVersionUID = -6801198449908409808L;

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid")
  private String id;

  @Column(name = "create_date")
  @JsonIgnore
  private Date createDate;

  @Column(name = "update_date")
  @JsonIgnore
  private Date updateDate;

  @PrePersist
  public void prePersist() {
    this.createDate = new Date();
  }

  @PreUpdate
  public void preUpdate() {
    updateDate = new Date();
  }
}
