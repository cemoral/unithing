package com.csemapps.unithing.entity;

import javax.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Location extends BaseEntity {

  private static final long serialVersionUID = -534975716432724871L;

  @Column(name = "name")
  private String name;
}
