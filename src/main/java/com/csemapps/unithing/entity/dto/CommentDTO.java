package com.csemapps.unithing.entity.dto;

import java.io.Serializable;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO implements Serializable {

  private static final long serialVersionUID = -3542006063411448673L;

  private String id;

  private String text;

  private String postId;
}
