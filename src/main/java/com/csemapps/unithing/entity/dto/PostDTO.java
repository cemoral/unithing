package com.csemapps.unithing.entity.dto;

import com.csemapps.unithing.entity.Location;
import java.io.Serializable;
import java.util.Date;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO implements Serializable {

  private static final long serialVersionUID = 647764363965189013L;

  private String id;

  private String text;

  private int upVote;

  private int downVote;

  private Location location;

  private Date createDate;
}
