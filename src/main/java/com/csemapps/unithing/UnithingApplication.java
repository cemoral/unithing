package com.csemapps.unithing;

import com.csemapps.unithing.entity.User;
import com.csemapps.unithing.service.UserService;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class UnithingApplication {

  public final UserService userService;

  public static void main(String[] args) {
    SpringApplication.run(UnithingApplication.class, args);
  }

  @PostConstruct
  public void init() {
    User userDto = userService.validateAndFindUser("user");
    if (userDto == null) {
      User user = new User("user", "user", "u", "u", "u@u.com", null, null, null, null, null);
      userService.createUser(user);
    }
  }
}
