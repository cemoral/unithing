package com.csemapps.unithing.exception;

import javax.security.auth.login.LoginException;

public class UsernameExistException extends LoginException {

  public UsernameExistException() {
    super();
  }

  public UsernameExistException(String msg) {
    super(msg);
  }
}
