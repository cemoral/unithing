package com.csemapps.unithing.exception;

public class PostNotValidException extends Exception {

  public PostNotValidException() {
    super();
  }

  public PostNotValidException(String message) {
    super(message);
  }
}
