DROP TABLE IF EXISTS location CASCADE;
CREATE TABLE location
(
    id          CHARACTER VARYING(255) NOT NULL PRIMARY KEY,
    create_date TIMESTAMP,
    update_date TIMESTAMP,
    name        CHARACTER VARYING(255) NOT NULL
);

DROP TABLE IF EXISTS uni_user CASCADE;
CREATE TABLE uni_user
(
    id          CHARACTER VARYING(255) NOT NULL PRIMARY KEY,
    create_date TIMESTAMP,
    update_date TIMESTAMP,
    username    CHARACTER VARYING(255) NOT NULL,
    password    CHARACTER VARYING(255) NOT NULL,
    first_name  CHARACTER VARYING(255),
    last_name   CHARACTER VARYING(255),
    email       CHARACTER VARYING(255),
    image_url   CHARACTER VARYING(255)
);

DROP TABLE IF EXISTS user_roles;
CREATE TABLE user_roles
(
    user_id character varying(255) NOT NULL REFERENCES uni_user (id),
    roles   character varying(255)
);

DROP TABLE IF EXISTS post CASCADE;
CREATE TABLE post
(
    id             CHARACTER VARYING(255) NOT NULL PRIMARY KEY,
    create_date    TIMESTAMP,
    update_date    TIMESTAMP,
    up_vote        INTEGER,
    down_vote      INTEGER,
    text           CHARACTER VARYING(255) NOT NULL,
    location_id_fk CHARACTER VARYING(255) REFERENCES location (id),
    user_id_fk     CHARACTER VARYING(255) NOT NULL REFERENCES uni_user (id)
);

DROP TABLE IF EXISTS comment CASCADE;
CREATE TABLE comment
(
    id          CHARACTER VARYING(255) NOT NULL PRIMARY KEY,
    create_date TIMESTAMP,
    update_date TIMESTAMP,
    text        CHARACTER VARYING(255) NOT NULL,
    user_id_fk  CHARACTER VARYING(255) NOT NULL REFERENCES uni_user (id),
    post_id_fk  CHARACTER VARYING(255) NOT NULL REFERENCES post (id)
);